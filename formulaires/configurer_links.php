<?php

/**
 * Formulaire configuration du plugin Links
 *
 * @plugin     Links
 * @copyright  2009-2019
 * @author     Collectif
 * @licence    GNU/GPL
 * @package    SPIP\Links\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_configurer_links_charger_dist() {

	$links = sql_fetsel('valeur', 'spip_meta', 'nom = "links"');
	$links = unserialize($links['valeur']);
	$valeur = [
		'style' => $links['style'] ?: '',
		'external' => $links['external'] ?: 'off',
		'download' => $links['download'] ?: 'off',
		'window' => $links['window'] ?: 'off',
		'doc_list' => $links['doc_list'] ?: '.pdf,.ppt,.xls,.doc'
	];
	return $valeur;
}

function formulaires_configurer_links_verifier_dist() {

	$erreurs = [];
	//Cas ou l'on veut des liens ouvrants sans rien choisir
	if ((_request('window') == 'on') && (!_request('external')) && (!_request('download'))) {
		$erreurs['window'] = _T('links:erreur_choisir_liens_ouvrants');
	}
	//Cas ou l'on veut des liens ouvrants sur les documents sans avoir specifier d'extension
	if ((_request('download')) && (!_request('doc_list'))) {
		$erreurs['doc_list'] = _T('links:erreur_choisir_extensions');
	}
	return $erreurs;
}

function formulaires_configurer_links_traiter_dist() {

	$links = serialize(['style' => _request('style'), 'window' => _request('window'), 'external' => _request('external'), 'download' => _request('download'), 'doc_list' => _request('doc_list')]);
	//Insere ou update ?
	if ($links_doc = sql_fetsel('valeur', 'spip_meta', 'nom = "links"')) {
		//On update
		sql_updateq('spip_meta', ['valeur' => $links, 'impt' => 'oui'], 'nom="links"');
		$res = ['message_ok' => _T('links:message_ok_update_configuration')];
	} else {
		//On insere
		$id = sql_insertq('spip_meta', ['nom' => 'links', 'valeur' => $links, 'impt' => 'oui']);
		$res = ['message_ok' => _T('links:message_ok_configuration')];
	}
	return $res;
}
